var fs = require('fs');
var util = require('util');
var {SourceMapGenerator, SourceNode} = require('source-map');
var loaderUtils = require('loader-utils');
var Parser = require('webpack/lib/Parser');

module.exports = function (source/*game.js content*/){
  var callback = this.async();
  var folder = loaderUtils.interpolateName(this, '[path]', {});
  concatenate.call(this, folder.slice(0,-1)).then(
    (processed) => callback(null, ...processed),
    (error) => callback(error)
  );
}

async function concatenate(folder) {
  var flattenDir = async (dir) => {
    var filesAndFolders = await util.promisify(fs.readdir)(dir);
    var foldersExpanded = await Promise.all(filesAndFolders.map((file) =>
      file.endsWith('.js') ? `${dir}/${file}` : flattenDir(`${dir}/${file}`)
    ));
    // flatten
    return [].concat(...foldersExpanded);
  }
  var files = await flattenDir(folder);
  var data = await Promise.all(files.map((f) => util.promisify(fs.readFile)(f)));

  // early syntax check for better error reporting
  for(let i=0; i<files.length; i++) {
    try {
      Parser.parse(data[i], {sourceType:'script'});
    } catch(e) {
      // pretty print parse error
      var lines = data[i].toString().split('\n');
      var range = [...Array(7).keys()].map((i) => e.loc.line - 4 + i);
      var preview = range.map((i) =>
          (i < 0 || i+1 > lines.length) ? '    •' : (
            String(i+1).padStart(3, ' ') + ' '
            + ( (i == 0) ? '┌' : (i+1 == lines.length) ? '└' : '│' )
            + (
                (i+2 == e.loc.line) ?
                lines[i].padEnd(e.loc.column+1, ' ').split('').map((char, j) => j == e.loc.column ? '╭─╮'+char : char).join('')
              : (i+1 == e.loc.line) ?
                lines[i].split('').map((char, j) => j == e.loc.column ? '│'+char+'│' : char).join('')
              : (i+0 == e.loc.line) ?
                lines[i].padEnd(e.loc.column+1, ' ').split('').map((char, j) => j == e.loc.column ? '╰─╯'+char : char).join('')
              : lines[i]
            )
          )
        ).join('\n');
      throw new SyntaxError(`\n\n${e.message}  in  ${files[i]}(:${e.loc.line}:${e.loc.column})\n\n${preview}\n`);
    }
  }

  // concat with delimiter
  var sourceOut = Buffer.concat([].concat(...data.map((buffer) => [buffer, Buffer.from('\n')])));

  // create sourcemap
  var sourceNode = new SourceNode();
  sourceNode.add(`//# sourceMappingURL=dev/js/game.js.map`);
  var line = 0;
  var smg = new SourceMapGenerator({file:'game_concat.js'});
  for(let  i=0; i<files.length; i++) {
    this.addDependency(files[i]);
    smg.setSourceContent(files[i], data[i].toString());
    var lineCount = data[i].toString().split('\n').length;
    for(let j = 0; j < lineCount; j++)
      smg.addMapping({
        source: files[i],
        original: { line: 1+j, column: 0 },
        generated: { line: 1+line++, column: 0 }
      });
  }

  return [sourceOut, smg.toJSON()];
}
