/**
 * @Author: thiemo
 * @Date:   2018-08-22T10:12:42+02:00
 * @Last modified by:   thiemo
 * @Last modified time: 2018-08-30T15:28:00+02:00
 */

const ProvidePlugin = require('webpack').ProvidePlugin;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin'); // clean build
const framework_root = __dirname;
const game_root = process.cwd();

// needed for `webpack --watch`
const imageassets = {};
const soundassets = {};

// the path(s) that should be cleaned
let pathsToClean = [
  'build'
];

// the clean options to use
let cleanOptions = {
  verbose: true,
  dry: false
};

let getMTLGModuls = function() {
  let ret = [];
  fs.readdirSync(`${game_root}/node_modules/`)
  // take only mtlg- folders
  .filter(folder => folder.startsWith("mtlg-"))
  // keep only those with a sw folder
  .filter(folder => fs.readdirSync(`./node_modules/${folder}`).includes("sw"))
  // add for each sw folder the copy statement to copy all js files
  .forEach((file, _) => {
    ret.push({
      from: `${game_root}/node_modules/${file}/sw/*.js`,
      to: `${game_root}/build/[name].[ext]`,
      test: /\.(js)$/
    });
  });
  return ret;
}


var config = {
  // bundle javascript
  entry: `concat-loader!${game_root}/dev/js/game.js`,
  output: {
    path: `${game_root}/build`,
    filename: 'game.js'
  },
  serve: {},
  resolve: { symlinks: false},
  module: { rules: [
    // bundle css
    { test: /\.css$/, loader: 'style-loader!css-loader' } ,
    // bundle framework and module assets (images and sounds)
    { test: new RegExp(`^${game_root}/.*/(img|sounds)/`.replace(/[/\\]/g, '[/\\\\]')),
      use: [ {
        loader: 'file-loader',
        options: { name: path => path.match(new RegExp(`^${game_root}/.*/((img|sounds)/.*)$`.replace(/[/\\]/g, '[/\\\\]')))[1] }
      } ] }
  ] },
  plugins: [
    new CleanWebpackPlugin(pathsToClean, cleanOptions),
    new ProvidePlugin({
      MTLG: 'mtlg-core',
      createjs: 'exports-loader?createjs!easeljs/lib/easeljs-NEXT.js',
      // TODO: workaround, remove window.createjs alias as soon as https://github.com/CreateJS/PreloadJS/issues/150 gets fixed
      'window.createjs': 'exports-loader?createjs!easeljs/lib/easeljs-NEXT.js'
    }),
    new CopyWebpackPlugin([
      // bundle manifest, game assets, index.html and html-dependencies
      {from: `${game_root}/dev/manifest`, to: `${game_root}/build/manifest`},
      {from: `${game_root}/dev/img`, to: `${game_root}/build/img`},
      {from: `${game_root}/dev/sounds`, to: `${game_root}/build/sounds`},
      {context: checkFonts(), from: `*.ttf`, to: `${game_root}/build/fonts`},
      //{from: `${game_root}/dev/fonts`, to: `${game_root}/build/fonts`},
      {from: `${framework_root}/build_essentials/index.html`, to: `${game_root}/build`},
      // external dependencies should be replaced with npm-requires
      {from: `${framework_root}/build_essentials/extern`, to: `${game_root}/build/lib`}
    ]),
    new CopyWebpackPlugin(getMTLGModuls()),
    // create image and sound manifests for preloading
    new ManifestPlugin({
        fileName: `${game_root}/build/imageassets.json`,
        seed: {},
        generate: (seed, files) => { files.forEach(({name, path}) => seed[name]=path); return Object.values(seed); },
        filter: f => /^img[/\\](?!\.gitkeep)/.test(f.path)
    }),
    new ManifestPlugin({
        fileName: `${game_root}/build/soundassets.json`,
        seed: {},
        generate: (seed, files) => { files.forEach(({name, path}) => seed[name]=path); return Object.values(seed); },
        filter: f => /^sounds[/\\](?!\.gitkeep)/.test(f.path)
    }),
    new ManifestPlugin({
        fileName: `${game_root}/build/fontassets.json`,
        seed: {},
        generate: (seed, files) => { files.forEach(({name, path}) => seed[name]=path); return Object.values(seed); },
        filter: f => /^fonts[/\\](?!\.gitkeep)/.test(f.path)
    })
  ],
  resolveLoader: {
    modules: [ 'node_modules', `${framework_root}/node_modules`, `${framework_root}/loaders` ]
  }
};

module.exports = (env, argv) => {
  if(!argv || !argv.mode)
    config.mode = 'development';
  if(!argv || !argv.mode || argv.mode == 'development')
    config.devtool = 'source-map';
  if(argv && argv.module)
    config.entry = `${game_root}/dev/js/game.js`;
  return config;
};

function checkFonts(){
  if(path.resolve(`${game_root}/dev`,`/fonts`)!=`/fonts`){
    return `${game_root}/dev/fonts`
  }else{
    return ``
  }
}
