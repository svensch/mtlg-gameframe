#!/usr/bin/env node

child_process = require('child_process');
fs = require('fs');
path = require('path');

framework_root = "./node_modules/mtlg-gameframe";
demos_root = "./node_modules/mtlg-modul-demos"

switch(process.argv[2]){
  case "new_game":
    copy(`${framework_root}/minDist/dev`);
    copy(`${framework_root}/minDist/.gitlab-ci.yml`);
    copyGitignore();
    copyPackageJson(`${framework_root}/minDist/package.json`);
    child_process.spawnSync('npm', ['install'], {stdio:'inherit', shell:true});
    break;
  case "demo_games":
    copy(`${demos_root}/demo/dev`);
    copyGitignore();
    copyPackageJson(`${demos_root}/demo/package.json`);
    child_process.spawnSync('npm', ['install'], {stdio:'inherit', shell:true});
    break;
  case "demo":
    // detect all available demos
    demos = {"core":"mtlg-core", "cli":"mtlg-gameframe"};
    // include downloaded modules with demos
    var modules = fs.readdirSync("./node_modules")
      // all downloaded mtlg modules
      .filter(folder => folder.startsWith("mtlg-modul"))
      // keep only those with a demo folder
      .filter(folder => fs.readdirSync(`./node_modules/${folder}`).includes("demo"))
      // extract short name
      .map(folder => [folder, folder.match(/mtlg-modul(?:e?)-(.*)/)[1]])
      // add to demos
      .forEach(([folder, shortname]) => demos[shortname] = folder);

    var name = process.argv[3];
    if(!(name in demos)) {
      console.log(`
The currently available demos are ${Object.keys(demos).join(', ')}. Setup a demo by using

mtlg demo <name>

or download a mtlg module with npm to get access to its demo.
`);
    } else {
      // copy and install selected demo
      var demo_folder = `./node_modules/${demos[name]}/demo`;
      copy(`${demo_folder}/dev`);
      copy(`${framework_root}/minDist/.gitlab-ci.yml`);
      copyGitignore();
      copyPackageJson(`${demo_folder}/package.json`);
      child_process.spawnSync('npm', ['install'], {stdio:'inherit', shell:true});
    }
    break;
  case "serve":
    // see https://github.com/webpack-contrib/webpack-serve#api
    var serve = require('webpack-serve');
    var module = process.argv.includes('module');
    // load config with current working directory (game root)
    var config = require(`${__dirname}/webpack.config.js`)(process.env, {module});
    // change dir for webpack-serve to work with linked mtlg-gameframe installation
    process.chdir(__dirname);
    serve({}, {config});
    break;
  case "build":
    var _argv = [process.argv[0], process.argv[1], '--config', `${__dirname}/webpack.config.js`];
    if(process.argv.includes('production'))
      _argv.push('--mode', 'production');
    if(process.argv.includes('module'))
      _argv.push('--module', 'true');
    process.argv = _argv;
    require('webpack-cli');
    break;
  default:
    console.log(`
usage: mtlg [new_game|min_game|demo|serve|build]

  new_game                       setup a new game template
  min_game                       setup a minimal working template
  demo [name]                    setup a game directory with the demo [name]
  serve [module]                 serves the game as a local webpage
                                 use *module* to build without concatenation
  build [module] [production]    creates a distributable game webpage
                                 use *module* to build without concatenation
                                 use *production* for a minimized webpage

    `);
}

function copy(src) {
  console.log(`copy ${path.basename(src)}`);
  // recursive copy
  var cp = (base, file) => {
    if(fs.statSync(file).isFile()) {
      fs.copyFileSync(file, path.relative(base, file));
    } else {
      // directory
      fs.mkdirSync(path.relative(base, file));
      fs.readdirSync(file).forEach(f => cp(base, `${file}/${f}`));
    }
  }
  cp(path.dirname(src), src);
}

function copyGitignore() {
  // '.gitignore' is sometimes renamed by npm (cf https://stackoverflow.com/questions/24976950)
  console.log('copy .gitignore');
  fs.copyFileSync(`${framework_root}/minDist/_gitignore`, '.gitignore');
}

function copyPackageJson(src) {
  console.log('apply package.json');
  // make sure MTLG dependency remains in package.json the way it was installed
  package_json = JSON.parse(fs.readFileSync(src));
  try {
    //second prio: fixed commit as reported in package.lock.json
    var framework_version = JSON.parse(fs.readFileSync('package-lock.json')).dependencies['mtlg-gameframe'].version
    //first prio: as defined in package.json
    framework_version = JSON.parse(fs.readFileSync('package.json')).dependencies['mtlg-gameframe']
  } catch (e) {}
  if (framework_version)
    package_json.dependencies = Object.assign({}, package_json.dependencies, {"mtlg-gameframe": framework_version});
  fs.writeFileSync('package.json', JSON.stringify(package_json, null, "  "))
}
